const axios = require('axios');

export default {
  retrieveAll() {
    const uri = `${this.baseUri()}/deliveryMethods`;
    const headers = {
      'Accept': 'application/json'
    }

    return axios.get(uri, null, headers)
      .then(function (response) {
        return response.data;
      })
      .catch(function (error) {
        console.log(error);
      });
  },

  create(attrs) {
    const uri = `${this.baseUri()}/deliveryMethods`;

    return axios.post(uri, attrs)
      .then(function (response) {
        return response.data;
      })
      .catch(function (error) {
        console.log(error);
      });
  },

  retrieve(deliveryMethodCode) {
    const uri = `${this.baseUri()}/deliveryMethods/${deliveryMethodCode}`;

    axios.get(uri)
      .then(function (response) {
        console.log(response);
      })
      .catch(function (error) {
        console.log(error);
      })
      .finally(function () {
        // always executed
      });
  },

  update(deliveryMethodCode, attrs) {
    const uri = `${this.baseUri()}/deliveryMethods/${deliveryMethodCode}`;

    return axios.patch(uri, attrs)
      .then(function (response) {
        return response.data;
      })
      .catch(function (error) {
        console.log(error);
      });
  },

  delete(deliveryMethodCode) {
    const uri = `${this.baseUri()}/deliveryMethods/${deliveryMethodCode}`;

    return axios.delete(uri)
      .then(function (response) {
        return response.data;
      })
      .catch(function (error) {
        console.log(error);
      });
  },

  baseUri() {
    const host = process.env.VUE_APP_EXTERNAL_API_HOST;
    const port = process.env.VUE_APP_EXTERNAL_API_PORT;

    return `http://${host}:${port}`;
  }
}
