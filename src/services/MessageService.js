const axios = require('axios');

export default {
  retrieveAll() {
    const uri = `${this.baseUri()}/messages`;
    const headers = {
      'Accept': 'application/json'
    }

    return axios.get(uri, null, headers)
      .then(function (response) {
        return response.data;
      })
      .catch(function (error) {
        console.log(error);
      });
  },

  create(attrs) {
    const uri = `${this.baseUri()}/messages`;

    return axios.post(uri, attrs)
      .then(function (response) {
        return response.data;
      })
      .catch(function (error) {
        console.log(error);
      });
  },

  retrieveById(messageId) {
    const uri = `${this.baseUri()}/messages/${messageId}`;

    axios.get(uri)
      .then(function (response) {
        console.log(response);
      })
      .catch(function (error) {
        console.log(error);
      })
      .finally(function () {
        // always executed
      });
  },

  baseUri() {
    const host = process.env.VUE_APP_EXTERNAL_API_HOST;
    const port = process.env.VUE_APP_EXTERNAL_API_PORT;

    return `http://${host}:${port}`;
  }
}
