const axios = require('axios');

export default {
  retrieveAll() {
    const uri = `${this.baseUri()}/adapters`;
    const headers = {
      'Accept': 'application/json'
    }

    return axios.get(uri, null, headers)
      .then(function (response) {
        return response.data;
      })
      .catch(function (error) {
        console.log(error);
      });
  },

  create(attrs) {
    const uri = `${this.baseUri()}/adapters`;

    return axios.post(uri, attrs)
      .then(function (response) {
        return response.data;
      })
      .catch(function (error) {
        console.log(error);
      });
  },

  retrieve(adapterCode) {
    const uri = `${this.baseUri()}/adapters/${adapterCode}`;

    axios.get(uri)
      .then(function (response) {
        console.log(response);
      })
      .catch(function (error) {
        console.log(error);
      })
      .finally(function () {
        // always executed
      });
  },

  update(adapterCode, attrs) {
    const uri = `${this.baseUri()}/adapters/${adapterCode}`;

    return axios.patch(uri, attrs)
      .then(function (response) {
        return response.data;
      })
      .catch(function (error) {
        console.log(error);
      });
  },

  delete(adapterCode) {
    const uri = `${this.baseUri()}/adapters/${adapterCode}`;

    return axios.delete(uri)
      .then(function (response) {
        return response.data;
      })
      .catch(function (error) {
        console.log(error);
      });
  },

  baseUri() {
    const host = process.env.VUE_APP_EXTERNAL_API_HOST;
    const port = process.env.VUE_APP_EXTERNAL_API_PORT;

    return `http://${host}:${port}`;
  }
}
