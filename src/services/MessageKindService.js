const axios = require('axios');

export default {
  retrieveAll() {
    const uri = `${this.baseUri()}/messageKinds`;
    const headers = {
      'Accept': 'application/json'
    }

    return axios.get(uri, null, headers)
      .then(function (response) {
        return response.data;
      })
      .catch(function (error) {
        console.log(error);
      });
  },

  create(attrs) {
    const uri = `${this.baseUri()}/messageKinds`;

    return axios.post(uri, attrs)
      .then(function (response) {
        return response.data;
      })
      .catch(function (error) {
        console.log(error);
      });
  },

  retrieve(messageKindCode) {
    const uri = `${this.baseUri()}/messageKinds/${messageKindCode}`;

    axios.get(uri)
      .then(function (response) {
        console.log(response);
      })
      .catch(function (error) {
        console.log(error);
      })
      .finally(function () {
        // always executed
      });
  },

  update(messageKindCode, attrs) {
    const uri = `${this.baseUri()}/messageKinds/${messageKindCode}`;

    return axios.patch(uri, attrs)
      .then(function (response) {
        return response.data;
      })
      .catch(function (error) {
        console.log(error);
      });
  },

  delete(messageKindCode) {
    const uri = `${this.baseUri()}/messageKinds/${messageKindCode}`;

    return axios.delete(uri)
      .then(function (response) {
        return response.data;
      })
      .catch(function (error) {
        console.log(error);
      });
  },

  baseUri() {
    const host = process.env.VUE_APP_EXTERNAL_API_HOST;
    const port = process.env.VUE_APP_EXTERNAL_API_PORT;

    return `http://${host}:${port}`;
  }
}
