const axios = require('axios');

export default {
  retrieveAll() {
    const uri = `${this.baseUri()}/deliveryKinds`;
    const headers = {
      'Accept': 'application/json'
    }

    return axios.get(uri, null, headers)
      .then(function (response) {
        return response.data;
      })
      .catch(function (error) {
        console.log(error);
      });
  },

  create(attrs) {
    const uri = `${this.baseUri()}/deliveryKinds`;

    return axios.post(uri, attrs)
      .then(function (response) {
        return response.data;
      })
      .catch(function (error) {
        console.log(error);
      });
  },

  retrieve(deliveryKindCode) {
    const uri = `${this.baseUri()}/deliveryKinds/${deliveryKindCode}`;

    axios.get(uri)
      .then(function (response) {
        console.log(response);
      })
      .catch(function (error) {
        console.log(error);
      })
      .finally(function () {
        // always executed
      });
  },

  update(deliveryKindCode, attrs) {
    const uri = `${this.baseUri()}/deliveryKinds/${deliveryKindCode}`;

    return axios.patch(uri, attrs)
      .then(function (response) {
        return response.data;
      })
      .catch(function (error) {
        console.log(error);
      });
  },

  delete(deliveryKindCode) {
    const uri = `${this.baseUri()}/deliveryKinds/${deliveryKindCode}`;

    return axios.delete(uri)
      .then(function (response) {
        return response.data;
      })
      .catch(function (error) {
        console.log(error);
      });
  },

  baseUri() {
    const host = process.env.VUE_APP_EXTERNAL_API_HOST;
    const port = process.env.VUE_APP_EXTERNAL_API_PORT;

    return `http://${host}:${port}`;
  }
}
