import Vue from 'vue';
import VueRouter from 'vue-router';
import vuetify from './plugins/vuetify';

import App from './App.vue';
import Home from './components/Home.vue'
import NotFoundComponent from './components/NotFoundComponent.vue'

import MessagePage from './components/message/MessagePage.vue'
import MessageKindPage from './components/messageKind/MessageKindPage.vue'
import AdapterPage from './components/adapter/AdapterPage.vue'
import DeliveryKindPage from './components/deliveryKind/DeliveryKindPage.vue'
import DeliveryMethodPage from './components/deliveryMethod/DeliveryMethodPage.vue'

Vue.use(VueRouter);

const router = new VueRouter({
  mode: 'history',
  base: __dirname,
  routes: [
    { path: '/', component: Home },
    { path: '/messages', component: MessagePage },
    { path: '/messageKinds', component: MessageKindPage },
    { path: '/adapters', component: AdapterPage },
    { path: '/deliveryKinds', component: DeliveryKindPage },
    { path: '/deliveryMethods', component: DeliveryMethodPage },
    { path: '*', component: NotFoundComponent }
  ]
});

Vue.config.productionTip = false;

new Vue({
  router,
  vuetify,
  render: h => h(App)
}).$mount('#app');
